package main

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/gorilla/schema"
	"html/template"
	"net/http"
	"net/smtp"
	"os"
	"path"
	"strings"
	textTemplate "text/template"
	"time"
)

var mimeMap = map[string]string{
	".css":   "text/css",
	".ico":   "image/x-icon",
	".jpg":   "image/jpeg",
	".js":    "text/javascript",
	".png":   "image/png",
	".svg":   "image/svg+xml",
	".wasm":  "application/wasm",
	".woff2": "font/woff2",
	".woff":  "font/woff",
}

var funcMap = template.FuncMap{
	"normalizeUsername": normalizeUsername,
}

var templates *template.Template

func loadTemplates() {
	templates = template.Must(template.New("*.tmpl").Funcs(funcMap).ParseGlob(path.Join(*root, "*.tmpl")))
}

var decoder = schema.NewDecoder()

func readForm(values interface{}, r *http.Request) (err error) {
	err = r.ParseForm()
	if err != nil {
		return
	}

	switch r.Method {
	case http.MethodGet:
		err = decoder.Decode(values, r.Form)
		if err != nil {
			return
		}
	case http.MethodPost:
		err = decoder.Decode(values, r.PostForm)
		if err != nil {
			return
		}
	default:
		err = errors.New("unsupported method: " + r.Method)
		return
	}

	if hasNil(values) {
		err = errors.New(fmt.Sprintf("missing parameter: %v", values))
		return
	}

	return
}

func (s *server) _protect(w http.ResponseWriter, r *http.Request, h HandlerFuncAuth, user, pass, file string) {
	if err := s.db.authenticate(user, pass); err != nil {
	logger.Notice(err.Error())

		serveLogin(w)
		return
	}

	if err := s.db.permitted(file, user); err != nil {
		dualErr := dualError{err: err, errPub: http.StatusForbidden}
		s.handleError(w, &dualErr, "project is restricted")
		return
	}

	switch file {
	case "ssh", "ssh2", "alias", "alias2":
		/* Setting up things on first login. */
		h(w, r, user)
	default:
		/* Ask for and install SSH key on first login. */
		sshAuthKeys, err := s.db.sshAuthKeys(user)
		if err != nil {
			dualErr := dualError{err: err, errPub: http.StatusInternalServerError}
			s.handleError(w, &dualErr, "")
			return
		}

		if sshAuthKeys == "" {
			http.Redirect(w, r, "/ssh", http.StatusSeeOther)
			logger.Notice("forwarding from " + file + " to ssh")
			return
		}

		/* Ask for alias on first login. */
		alias, err := s.db.alias(user)
		if err != nil {
			dualErr := dualError{err: err, errPub: http.StatusInternalServerError}
			s.handleError(w, &dualErr, "")
			return
		}

		if alias == "" {
			http.Redirect(w, r, "/alias", http.StatusSeeOther)
			logger.Notice("forwarding from " + file + " to alias")
			return
		}

		h(w, r, user)
	}
}

/* Serves differently depending on if logged in or not. */
func (s *server) mirror(hUnauth http.HandlerFunc, hAuth HandlerFuncAuth) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		file := r.URL.Path[1:]

		user, pass, ok := r.BasicAuth()
		if !ok {
			if err := s.db.permitted(file, ""); err != nil {
				dualErr := dualError{err: err, errPub: http.StatusForbidden}
				s.handleError(w, &dualErr, "project is restricted")
				return
			}

			hUnauth(w, r)
		} else {
			s._protect(w, r, hAuth, user, pass, file)
		}
	}
}

/* Serves if logged in; otherwise prompts for credentials. */
func (s *server) protect(h HandlerFuncAuth) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		user, pass, ok := r.BasicAuth()
		if !ok {
			serveLogin(w)
			return
		}

		s._protect(w, r, h, user, pass, r.URL.Path[1:])
	}
}

func (s *server) handleError(w http.ResponseWriter, err *dualError, msg string) {
	if msg != "" {
		http.Error(w, errorMap[err.errorPub()] + ": " + msg, err.errorPub())
		logger.Err(err.Error() + ": " + msg)
	} else {
		http.Error(w, errorMap[err.errorPub()], err.errorPub())
		logger.Err(err.Error())
	}
}

func (s *server) handleRestUnauth() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		if r.URL.Path == "/" {
			http.Redirect(w, r, "/landing", http.StatusSeeOther)
			logger.Notice("forwarding from / to landing")
			return
		}

		file := r.URL.Path[1:]

		if templates.Lookup(file) == nil {
			err := errors.New("unknown template: " + file)
			s.handleError(w, &dualError{err: err, errPub: http.StatusNotFound}, "")
			return
		} else {
			values := map[string]interface{}{
				"gitHost": gitHost(r.Host),
				"root": conf.Root,
			}

			if err := serve(w, r, file, values); err != nil {
				s.handleError(w, err, "")
			}
		}
	}
}

func (s *server) handleIndexUnauth() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		http.Redirect(w, r, "/landing", http.StatusSeeOther)
		logger.Notice("forwarding from " + r.URL.Path[1:] + " to landing")
	}
}

func (s *server) handleLoginUnauth() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		serveLogin(w)
	}
}

func (s *server) handleLanding() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		if err := serve(w, r, "landing", nil); err != nil {
			s.handleError(w, err, "")
		}
	}
}

func (s *server) handleRegisterReset2(w http.ResponseWriter, r *http.Request, op string, f func(string) error) {
	var emailBodyFilled bytes.Buffer

	values := new(struct {
		Student *email `schema:"student"`
	})

	err := readForm(values, r)
	if err != nil {
		s.handleError(w, &dualError{err: err, errPub: http.StatusBadRequest}, "")
		return
	}

	if !allowedEmailDomain(values.Student.String()) {
		msg := "disallowed email domain"
		s.handleError(w, &dualError{err: err, errPub: http.StatusBadRequest}, msg)
		return
	}

	dir := path.Join("/etc/httpd/accounts", values.Student.String())
	if err := f(dir); err != nil {
		s.handleError(w, &dualError{err: err,
					    errPub: http.StatusBadRequest}, err.Error())
		return
	}

	p := path.Join(*root, "email-" + op)
	tmpl, err := textTemplate.New(path.Base(p)).ParseFiles(p)
	if err != nil {
		s.handleError(w, &dualError{err: err, errPub: http.StatusInternalServerError}, "")
		return
	}

	uuid, err := uuid()
	if err != nil {
		s.handleError(w, &dualError{err: err, errPub: http.StatusInternalServerError}, "")
		return
	}

	pass, err := password()
	if err != nil {
		s.handleError(w, &dualError{err: err, errPub: http.StatusInternalServerError}, "")
		return
	}

	nonce := nonce()
	token := nonce + values.Student.String() + secret

	err = tmpl.Execute(&emailBodyFilled, map[string]interface{}{
		"date": time.Now().Format(time.RFC1123Z),
		"from": conf.EmailSender,
		"fromName": conf.EmailSenderName,
		"host": r.Host,
		"nonce": nonce,
		"token": hashCalc(token),
		"student": values.Student.String(),
		"password": pass,
		"uuid": uuid})
	if err != nil {
		s.handleError(w, &dualError{err: err, errPub: http.StatusInternalServerError}, "")
		return
	}

	logger.Notice("preparing " + op + " email" +
		      " student: "  + values.Student.String() +
		      " nonce: "    + nonce +
		      " token: "    + hashCalc(token) +
		      " password: " + pass)

	if strings.HasSuffix(values.Student.String(), "example.com") {
		msg := "example.com; assuming test thus not sending email"
		err := errors.New("disallowed email domain")
		s.handleError(w, &dualError{err: err, errPub: http.StatusBadRequest}, msg)
		return
	}

	err = smtp.SendMail(conf.EmailRelay, nil, conf.EmailSender,
			  []string{values.Student.String()},
			  []byte(emailBodyFilled.Bytes()))
	if err != nil {
		s.handleError(w, &dualError{err: err, errPub: http.StatusInternalServerError}, "")
		return
	}

	w.Header().Set("Content-Type", "text/html")

	err = templates.ExecuteTemplate(w, op + "2", map[string]interface{}{"student": values.Student.String()})
	if err != nil {
		s.handleError(w, &dualError{err: err, errPub: http.StatusInternalServerError}, "")
		return
	}

	logger.Notice("not authenticated: provided " + op + "2")

	return
}

func (s *server) handleRegisterReset3(w http.ResponseWriter, r *http.Request, op string, f func(string, string) (string, error)) {
	values := new(struct {
		Student     *email      `schema:"student"`
		Nonce       *safeString `schema:"nonce"`
		HashedToken *safeString `schema:"token"`
		Password    *safeString `schema:"password"`
	})

	err := readForm(values, r)
	if err != nil {
		s.handleError(w, &dualError{err: err, errPub: http.StatusBadRequest}, "")
		return
	}

	token := values.Nonce.String() + values.Student.String() + secret
	if !hashSame(values.HashedToken.String(), hashCalc(token)) {
		msg := "faulty authentication; is token expired?"
		err = errors.New(msg)
		s.handleError(w, &dualError{err: err, errPub: http.StatusBadRequest}, msg)
		return
	}

	logger.Notice("received valid authentication to " + op + "3")
	id, err := f(values.Student.String(), values.Password.String())
	if err != nil {
		s.handleError(w, &dualError{err: err, errPub: http.StatusInternalServerError}, "")
		return
	}

	w.Header().Set("Content-Type", "text/html")

	err = templates.ExecuteTemplate(w, op + "3", map[string]interface{}{"uuid": id})
	if err != nil {
		s.handleError(w, &dualError{err: err, errPub: http.StatusInternalServerError}, "")
		return
	}

	logger.Notice("not authenticated: provided " + op + "3")

	return
}

func (s *server) handleRegister() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		if err := serve(w, r, "register", nil); err != nil {
			s.handleError(w, err, "")
		}
	}
}

func (s *server) handleRegister2() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		s.handleRegisterReset2(w, r, "register", func(dir string) (err error) {
			if _, err = os.Stat(dir); err == nil {
				err = errors.New("student exists")
			} else {
				err = nil
			}
			return
		})
	}
}

func (s *server) handleRegister3() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		s.handleRegisterReset3(w, r, "register", func(student, password string) (uuid string, err error) {
			return s.db.addStudent(student, password)
		})
	}
}

func (s *server) handleReset() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		if err := serve(w, r, "reset", nil); err != nil {
			s.handleError(w, err, "")
		}
	}
}

func (s *server) handleReset2() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		s.handleRegisterReset2(w, r, "reset", func(dir string) (err error) {
			if _, err = os.Stat(dir); err != nil {
				err = errors.New("student does not exist")
			} else {
				err = nil
			}
			return
		})
	}
}

func (s *server) handleReset3() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		s.handleRegisterReset3(w, r, "reset", func(student, password string) (uuid string, err error) {
			/* Kind of silly, but setPassword is synchronous; there is no UUID. */
			return "", s.db.setPassword(student, password)
		})
	}
}

func (s *server) handleStatic() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		mimeType, ok := mimeMap[path.Ext(r.URL.Path)]
		if ok {
			w.Header().Set("Content-Type", mimeType)
		} else {
			w.Header().Set("Content-Type", "application/octet-stream")
		}

		p := path.Join(*root, r.URL.Path[1:])

		if _, err := os.Stat(p + ".gz"); err == nil {
			w.Header().Set("Content-Encoding", "gzip")
			http.ServeFile(w, r, p + ".gz")
		} else {
			http.ServeFile(w, r, p)
		}
	}
}

func (s *server) handleRestAuth() HandlerFuncAuth {
	return func(w http.ResponseWriter, r *http.Request, user string) {
		if r.URL.Path == "/" {
			http.Redirect(w, r, "/projects", http.StatusSeeOther)
			logger.Notice("forwarding from / to projects")
			return
		}

		file := r.URL.Path[1:]

		if templates.Lookup(file) == nil {
			err := errors.New("unknown template: " + file)
			s.handleError(w, &dualError{err: err, errPub: http.StatusNotFound}, "")
			return
		} else {
			last, err := s.db.last(user, file)
			if err != nil {
				dualErr := dualError{err: err, errPub: http.StatusInternalServerError}
				s.handleError(w, &dualErr, "")
				return
			}

			values := map[string]interface{}{
				"auth": true,
				"teacher": s.db.isTeacher(user),
				"gitHost": gitHost(r.Host),
				"student": user,
				"project": file,
				"last": last,
				"root": conf.Root,
			}

			if err := serve(w, r, file, values); err != nil {
				s.handleError(w, err, "")
			}
		}
	}
}

func (s *server) handleIndexAuth() HandlerFuncAuth {
	return func(w http.ResponseWriter, r *http.Request, user string) {
		http.Redirect(w, r, "/projects", http.StatusSeeOther)
		logger.Notice("forwarding from " + r.URL.Path[1:] + " to projects")
	}
}

func (s *server) handleLoginAuth() HandlerFuncAuth {
	return func(w http.ResponseWriter, r *http.Request, user string) {
		/* Post-authentication request following first load of login. */
		http.Redirect(w, r, "/", http.StatusSeeOther)
		logger.Notice("forwarding from login to projects")
	}
}

func (s *server) handleSsh() HandlerFuncAuth {
	return func(w http.ResponseWriter, r *http.Request, user string) {
		sshAuthKeys, err := s.db.sshAuthKeys(user)
		if err != nil {
			dualErr := dualError{err: err, errPub: http.StatusInternalServerError}
			s.handleError(w, &dualErr, "")
			return
		}

		values := map[string]interface{}{
			"auth": true,
			"teacher": s.db.isTeacher(user),
			"authorizedKeys": sshAuthKeys,
			"student": user,
		}

		if err := serve(w, r, "ssh", values); err != nil {
			s.handleError(w, err, "")
		}
	}
}

func (s *server) handleSsh2() HandlerFuncAuth {
	return func(w http.ResponseWriter, r *http.Request, user string) {
		values := new(struct {
			Key *key `schema:"key"`
		})

		err := readForm(values, r)
		if err != nil {
			s.handleError(w, &dualError{err: err, errPub: http.StatusBadRequest}, "")
			return
		}

		logger.Notice("received public key " + values.Key.String())

		id, err := s.db.setSshAuthKeys(user, values.Key.String())
		if err != nil {
			s.handleError(w, &dualError{err: err, errPub: http.StatusInternalServerError}, "")
			return
		}

		if err := serve(w, r, "ssh2", map[string]interface{}{
			"auth": true,
			"teacher": s.db.isTeacher(user),
			"student": user,
			"uuid": id,
		}); err != nil {
			s.handleError(w, err, "")
			return
		}
	}
}

func (s *server) handleWasmUnauth() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		file := r.URL.Path[1:]

		values := map[string]interface{}{
			"auth": false,
			"teacher": false,
			"student": "",
			"wasm": file,
		}

		if err := serve(w, r, "wasm", values); err != nil {
			s.handleError(w, err, "")
		}
	}
}

func (s *server) handleWasmAuth() HandlerFuncAuth {
	return func(w http.ResponseWriter, r *http.Request, user string) {
		file := r.URL.Path[1:]

		values := map[string]interface{}{
			"auth": true,
			"teacher": s.db.isTeacher(user),
			"student": user,
			"wasm": file,
		}

		if err := serve(w, r, "wasm", values); err != nil {
			s.handleError(w, err, "")
		}
	}
}

func (s *server) handleRemove() HandlerFuncAuth {
	return func(w http.ResponseWriter, r *http.Request, user string) {
		values := map[string]interface{}{
			"auth": true,
			"teacher": s.db.isTeacher(user),
			"student": user,
		}

		if err := serve(w, r, "remove", values); err != nil {
			s.handleError(w, err, "")
		}
	}
}

func (s *server) handleRemove2() HandlerFuncAuth {
	return func(w http.ResponseWriter, r *http.Request, user string) {
		values := new(struct {
			Student *email `schema:"student"`
		})

		err := readForm(values, r)
		if err != nil {
			s.handleError(w, &dualError{err: err, errPub: http.StatusBadRequest}, "")
			return
		}

		if values.Student.String() != user {
			s.handleError(w, &dualError{err: err, errPub: http.StatusBadRequest}, "")
			return
		}

		logger.Notice("received request to remove " + values.Student.String())

		id, err := s.db.removeStudent(user)
		if err != nil {
			s.handleError(w, &dualError{err: err, errPub: http.StatusInternalServerError}, "")
			return
		}

		if err := serve(w, r, "logout", map[string]interface{}{"uuid": id}); err != nil {
			s.handleError(w, err, "")
			return
		}
	}
}

func (s *server) handlePassword() HandlerFuncAuth {
	return func(w http.ResponseWriter, r *http.Request, user string) {
		values := map[string]interface{}{
			"auth": true,
			"teacher": s.db.isTeacher(user),
			"student": user,
		}

		if err := serve(w, r, "password", values); err != nil {
			s.handleError(w, err, "")
		}
	}
}

func (s *server) handlePassword2() HandlerFuncAuth {
	return func(w http.ResponseWriter, r *http.Request, user string) {
		values := new(struct {
			Password1 *safeString `schema:"password1"`
			Password2 *safeString `schema:"password2"`
		})

		err := readForm(values, r)
		if err != nil {
			s.handleError(w, &dualError{err: err, errPub: http.StatusBadRequest}, "")
			return
		}

		if values.Password1.String() != values.Password2.String() {
			msg := "password entries do not match"
			http.Error(w, msg, http.StatusInternalServerError)
			logger.Err(msg)
			return
		}

		logger.Notice("received new password")

		if err := s.db.setPassword(user, values.Password1.String()); err != nil {
			s.handleError(w, &dualError{err: err, errPub: http.StatusInternalServerError}, "")
			return
		}

		if err := serve(w, r, "password2", map[string]interface{}{
			"auth": true,
			"teacher": s.db.isTeacher(user),
			"student": user,
		}); err != nil {
			s.handleError(w, err, "")
			return
		}
	}
}

func (s *server) handleAlias() HandlerFuncAuth {
	return func(w http.ResponseWriter, r *http.Request, user string) {
		alias, err := s.db.alias(user)
		if err != nil {
			dualErr := dualError{err: err, errPub: http.StatusInternalServerError}
			s.handleError(w, &dualErr, "")
			return
		}

		values := map[string]interface{}{
			"auth": true,
			"teacher": s.db.isTeacher(user),
			"alias": alias,
			"student": user,
		}

		if err := serve(w, r, "alias", values); err != nil {
			s.handleError(w, err, "")
		}
	}
}

func (s *server) handleAlias2() HandlerFuncAuth {
	return func(w http.ResponseWriter, r *http.Request, user string) {
		values := new(struct {
			Alias *safeString `schema:"alias"`
		})

		err := readForm(values, r)
		if err != nil {
			s.handleError(w, &dualError{err: err, errPub: http.StatusBadRequest}, "")
			return
		}

		logger.Notice("received new alias")

		if err := s.db.setAlias(user, values.Alias.String()); err != nil {
			if err == errAliasExists {
				s.handleError(w, &dualError{err: err, errPub: http.StatusBadRequest}, err.Error())
			} else {
				s.handleError(w, &dualError{err: err, errPub: http.StatusInternalServerError}, "")
			}
			return
		}

		if err := serve(w, r, "alias2", map[string]interface{}{
			"auth": true,
			"teacher": s.db.isTeacher(user),
			"student": user,
		}); err != nil {
			s.handleError(w, err, "")
			return
		}
	}
}

func (s *server) handleReferenceUnauth() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		if err := serve(w, r, "references", nil); err != nil {
			s.handleError(w, err, "")
		}
	}
}

func (s *server) handleReferenceAuth() HandlerFuncAuth {
	return func(w http.ResponseWriter, r *http.Request, user string) {
		values := map[string]interface{}{
			"auth": true,
			"teacher": s.db.isTeacher(user),
			"student": user,
		}

		if err := serve(w, r, "references", values); err != nil {
			s.handleError(w, err, "")
		}
	}
}

func (s *server) handleBook() HandlerFuncAuth {
	return func(w http.ResponseWriter, r *http.Request, user string) {
		values := map[string]interface{}{
			"auth": true,
			"teacher": s.db.isTeacher(user),
			"student": user,
			"wasm": "book",
		}

		if !s.db.isTeacher(user) {
			err := errors.New("non-teacher loaded book")
			s.handleError(w, &dualError{err: err, errPub: http.StatusForbidden}, "")
			return
		}

		if err := serve(w, r, "wasm", values); err != nil {
			s.handleError(w, err, "")
		}
	}
}

func (s *server) handleGrades() HandlerFuncAuth {
	return func(w http.ResponseWriter, r *http.Request, user string) {
		values := map[string]interface{}{
			"auth": true,
			"teacher": s.db.isTeacher(user),
			"student": user,
		}

		if err := serve(w, r, "grades", values); err != nil {
			s.handleError(w, err, "")
		}
	}
}

func (s *server) handleApiGrades() HandlerFuncAuth {
	return func(w http.ResponseWriter, r *http.Request, user string) {
		w.Header().Set("Content-Type", "application/json")

		values := new(struct {
			Student *safeString `schema:"student"`
			Project *safeString `schema:"project"`
			Brief   *safeString `schema:"brief"`
		})

		err := readForm(values, r)
		if err != nil {
			s.handleError(w, &dualError{err: err, errPub: http.StatusBadRequest}, "")
			return
		}

		grades, err := s.db.grades(
			user,
			values.Student.String(),
			values.Project.String(),
			values.Brief.String())
		if err != nil {
			s.handleError(w, &dualError{err: err, errPub: http.StatusInternalServerError}, "")
			return
		}

		enc := json.NewEncoder(w)

		err = enc.Encode(grades)
		if err != nil {
			s.handleError(w, &dualError{err: err, errPub: http.StatusInternalServerError}, "")
			return
		}
	}
}

func (s *server) handleLogout() HandlerFuncAuth {
	return func(w http.ResponseWriter, r *http.Request, user string) {
		w.WriteHeader(http.StatusUnauthorized)
		serve(w, r, "logout", nil)
	}
}

func serveLogin(w http.ResponseWriter) {
	w.Header().Set("WWW-Authenticate", `Basic realm="Restricted"`)
	http.Error(w, "unauthorized", http.StatusUnauthorized)
	logger.Notice("not authenticated: requesting credentials")
}

func serve(w http.ResponseWriter, req *http.Request, file string, values map[string]interface{}) (dualErr *dualError) {
	w.Header().Set("Content-Type", "text/html")

	if req.Host == "" {
		msg := "no host value in request"
		return &dualError{err: errors.New(msg), errPub: http.StatusBadRequest}
	}

	err := templates.ExecuteTemplate(w, file, values)
	if err != nil {
		return &dualError{err: err, errPub: http.StatusInternalServerError}
	}

	return nil
}
