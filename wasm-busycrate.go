package main

import (
	"os"
)

func wait() {
	<-make(chan struct{}, 0)
}

func main() {
	switch os.Args[0] {
		case "book":
			book(os.Args[1])
		case "challenge":
			challenge(os.Args[1])
		case "projects":
			projects(os.Args[1])
		case "rankings":
			rankings(os.Args[1])
	}

	wait()
}
