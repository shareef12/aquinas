package main

import (
	"testing"
)

func TestRenderLists(t *testing.T) {
	s1 := "no one"
	s2 := renderList(nil)
	if s2 != s1 {
		t.Error("failed to render as " + s1 + ", got " + s2)
	}

	s1 = "no one"
	s2 = renderList([]string{})
	if s2 != s1 {
		t.Error("failed to render as " + s1 + ", got " + s2)
	}

	s1 = "x"
	s2 = renderList([]string{"x"})
	if s2 != s1 {
		t.Error("failed to render as " + s1 + ", got " + s2)
	}

	s1 = "x and y"
	s2 = renderList([]string{"x", "y"})
	if s2 != s1 {
		t.Error("failed to render as " + s1 + ", got " + s2)
	}

	s1 = "x, y, and z"
	s2 = renderList([]string{"x", "y", "z"})
	if s2 != s1 {
		t.Error("failed to render as " + s1 + ", got " + s2)
	}
}
