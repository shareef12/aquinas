package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path"
)

const CONFIG      = "aquinas-build.json"
const DESCRIPTION = "description.json"

type config struct {
	Projects string `json:projects`
}

func stringInSlice(a string, list []string) bool {
	for _, b := range list {
		if b == a {
			return true
		}
	}
	return false
}

func main() {
	var config config
	var nodes []Node
	projects := make(map[string]project)

	f, err := os.Open(CONFIG)
	if err != nil {
		log.Fatal(err)
	}

	dec := json.NewDecoder(f)

	if err := dec.Decode(&config); err != nil {
		log.Fatal(err)
	}

	files, err := ioutil.ReadDir(config.Projects)
	if err != nil {
		log.Fatal(err)
	}

	for _, file := range files {
		if file.Name()[0] == '.' ||
		   file.Name() == "references.bib" ||
		   file.Name() == "Makefile" {
			continue
		}

		var project project
		prerequisites := make(map[string]bool)

		f, err := os.Open(path.Join(config.Projects, file.Name(), DESCRIPTION))
		if err != nil {
			log.Fatal(err)
		}

		dec := json.NewDecoder(f)

		if err := dec.Decode(&project); err != nil {
			log.Fatal(err)
		}

		for _, v := range project.Prerequisites {
			prerequisites[v] = true
		}

		nodes = append(nodes, Node{ Name: project.Name, Prereq: prerequisites })
		projects[project.Name] = project
	}

	sorted, err := kahn(nodes)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("package main")
	fmt.Println("")
	fmt.Println("type projectListItem struct {")
	fmt.Println("	name string")
	fmt.Println("	language string")
	fmt.Println("	tags []string")
	fmt.Println("	summary string")
	fmt.Println("	link string")
	fmt.Println("}")
	fmt.Println("")
	fmt.Println("var projectList = []projectListItem{")
	for _, name := range sorted {
		for _, lang := range projects[name].Languages {
			var name2, link string
			if lang == "none" {
				name2 = name
				link = name
			} else {
				name2 = name + " in " + lang
				link = name + lang
			}
			fmt.Println("	{")
			fmt.Printf("		name: \"%s\",\n", name2)
			fmt.Printf("		language: \"%s\",\n", lang)
			fmt.Printf("		tags: []string{\n")
			for _, tag := range projects[name].Tags {
				fmt.Printf("			\"%s\",\n", tag)
			}
			fmt.Printf("		},\n")
			fmt.Printf("		summary: \"%s\",\n", projects[name].Summary)
			fmt.Printf("		link: \"%s\",\n", link)
			fmt.Println("	},")
		}
	}
	fmt.Println("}")
}
