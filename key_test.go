package main

import (
	"testing"
)

func TestSafeKey(t *testing.T) {
	d := []byte("ssh-rsa SGVsbG8sIHdvcmxkIQo= user@example.com")
	var k key

	if err := k.UnmarshalText(d); err != nil {
		t.Error(err.Error())
	}
	if string(d) != k.String() {
		t.Error("safeKey modified")
	}
}

func TestSafeKeySpaces(t *testing.T) {
	d := []byte("  ssh-rsa SGVsbG8sIHdvcmxkIQo= user@example.com  ")
	var k key

	if err := k.UnmarshalText(d); err != nil {
		t.Error(err.Error())
	}
}

func TestSafeKeyNewlines(t *testing.T) {
	d := []byte("\n\nssh-rsa SGVsbG8sIHdvcmxkIQo= user@example.com\n\n")
	var k key

	if err := k.UnmarshalText(d); err != nil {
		t.Error(err.Error())
	}
}
