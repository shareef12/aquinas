package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
	"syscall/js"
)

func renderBook(this js.Value, args []js.Value) interface{} {
	data := args[0].String()

	document := js.Global().Get("document")
	ul := document.Call("getElementById", "main-content-ul")
	ul.Set("innerHTML", "")

	input := document.Call("getElementById", "main-content-search")
	search := input.Get("value").String()

	grades := []grade{}
	json.Unmarshal([]byte(data), &grades)

	for _, g := range grades {
		if !strings.Contains(g.Student, search) &&
		   !strings.Contains(g.Project, search) &&
		   !strings.Contains(g.Outcome, search) &&
		   !strings.Contains(g.Timestamp, search) {
			continue
		}

		li := document.Call("createElement", "li")
		record := fmt.Sprintf("%s: %s %s %s", g.Student, g.Project, g.Outcome, g.Timestamp)
		li.Call("appendChild", document.Call("createTextNode", record))
		ul.Call("appendChild", li)
	}

	return nil
}

func book(student string) {
	title := "Aquinas: Grade Book"
	descr := "List of student grades"

	resp, err := http.Get("/api/grades?student=&project=&brief=true")
	if err != nil {
		fmt.Println(err.Error())
		return
	}
	defer resp.Body.Close()

	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println(err.Error())
		return
	}

	grades := []grade{}
	json.Unmarshal(data, &grades)

	document := js.Global().Get("document")
	document.Set("title", title)

	t := document.Call("getElementsByClassName", "page-title").Index(0)
	t.Set("innerHTML", title)

	d := document.Call("getElementsByClassName", "page-description").Index(0)
	d.Set("innerHTML", descr)

	content := document.Call("getElementsByClassName", "main-content").Index(0)

	label := document.Call("createElement", "label")
	label.Set("innerHTML", "Search:")
	content.Call("appendChild", label)

	input := document.Call("createElement", "input")
	input.Set("type", "text")
	input.Set("id", "main-content-search")
	input.Call("addEventListener", "input", js.FuncOf(
		func(this js.Value, args []js.Value) interface{} {
			js.FuncOf(renderBook).Invoke(string(data))
			return nil
		}))
	content.Call("appendChild", input)

	ul := document.Call("createElement", "ul")
	ul.Set("id", "main-content-ul")
	content.Call("appendChild", ul)

	js.FuncOf(renderBook).Invoke(string(data))
}
