package main

import (
	"regexp"
	"net/mail"
)

type email struct {
	*mail.Address
}

func allowedEmailDomain(email string) bool {
	/* Allow example.com allows for testing; see test-case-battery-07-www-register. */
	allowed := regexp.MustCompile(conf.EmailRegex)
	if allowed.Match([]byte(email)) {
		return true
	}

	return false
}

func (e *email) UnmarshalText(text []byte) (err error) {
	e.Address, err = mail.ParseAddress(string(text))
	if err != nil {
		return
	}

	if _, err = safe(e.Address.Address); err != nil {
		return
	}

	return
}

func (e *email) String() string {
	return e.Address.Address
}
