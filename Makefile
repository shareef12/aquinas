# := speeds things up by avoiding recursive expansion.
domain        := $(shell jq -r .domain aquinas.json)
projects-repo := $(shell jq -r .projects aquinas-build.json)
webhost       := aquinas-www.$(domain)
webadmin      := teacher

# C99, because otherwise cpp defines "linux" as "1."
cpp = cpp -traditional-cpp -C -P -nostdinc -std=c99 -Werror -Iwww

hosts= \
	aquinas-git \
	aquinas-www \
	aquinas-target \
	aquinas-user \

# Build with ext4 security labels for use with libcap-bin.
# To generate a patch such as this one, enter the OpenWrt tree and
# (1) run "make kernel_menuconfig" and (2) run "git diff target/linux."
aquinas-www_openwrt_build_opts=-k openwrt-build/definitions/aquinas-www.kernel-config-patch

openwrt-build=~/Source/openwrt-build/openwrt-build
files-common=~/Source/openwrt-build-$(domain)/files/common

# These are not generated from TeX. They are either static or generated
# using a custom rule.
_tmpls = \
	www/alias.tmpl \
	www/alias2.tmpl \
	www/bottom.tmpl \
	www/grades.tmpl \
	www/landing.tmpl \
	www/logout.tmpl \
	www/menu.tmpl \
	www/password2.tmpl \
	www/password.tmpl \
	www/records.tmpl \
	www/register2.tmpl \
	www/register3.tmpl \
	www/register.tmpl \
	www/remove.tmpl \
	www/reset2.tmpl \
	www/reset3.tmpl \
	www/reset.tmpl \
	www/ssh2.tmpl \
	www/ssh.tmpl \
	www/top.tmpl \
	www/wasm.tmpl

emails = \
	www/email-register \
	www/email-reset

extension_AMD64 = .S
extension_C = .c
extension_Go = .go

langsuffix = $(if $(findstring none,$(1)),,$(1))

css = \
	www/css/aquinas.css \
	www/css/LaTeXML.css \
	www/css/ltx-amsart.css \
	www/css/ltx-article.css \
	www/css/ltx-listings.css

all: list tmpls _tmpls programs wasms installs conf # check

.PHONY: deps
deps:
	go get github.com/gorilla/schema

.PHONY: check
check:
	for i in $(tmpls) $(_tmpls); do \
		tidy -utf8 -m -q -errors $$i || exit 1; \
	done
	linkchecker --threads=-1 --no-warnings $(tmpls) $(_tmpls)

# macOS ls does not support -I.
projects := $(shell ls $(projects-repo) | grep -v "Makefile\|references.bib")

define languages
$(shell jq -r '.languages[] // empty' $(projects-repo)/$(1)/description.json)
endef

define prerequisites
$(shell jq -r 'select(.prerequisites != null) | .prerequisites[] // empty' $(projects-repo)/$(1)/description.json)
endef

define summary
$(shell jq -r '.summary // empty' $(projects-repo)/$(1)/description.json)
endef

# No getent on macOS.
ifneq ($(shell which getent),)
service_ip := $(shell getent ahostsv4 aquinas-target.$(domain) | grep STREAM | cut -d ' ' -f 1)
else
service_ip := 127.0.0.1
endif

define service_sources
$(shell jq -r 'select(.services != null) | .services[].source // empty' $(projects-repo)/$(1)/description.json)
endef

define service_ports
$(shell jq -r 'select(.services != null) | .services[].port // empty' $(projects-repo)/$(1)/description.json)
endef

www/css/aquinas.css: www/less/style.less
	lessc --js $^ $@

define BUILDSERVICESRC_template
ifeq ($(findstring www/$(2),$(cleans)),)
cleans += www/$(2)

www/$(2): $(projects-repo)/$(1)/$(2)
	./aquinas-sanitize $$< >$$@
endif
endef

define BUILDRECORD_template
# $(1): project
# $(2): language
cleans += www/$(1)$(2).tex
cleans += www/$(1)$(2).checked
cleans += www/$(1)$(2)-title.tex
cleans += www/$(1)$(2)-summary.tex
cleans += www/$(1)$(2)-project.tex
cleans += www/$(1)$(2)-language.tex
cleans += www/$(1)$(2)-prerequisites.tex
cleans += www/$(1)$(2)-extension.tex
cleans += www/$(1)$(2)-service-ip.tex
cleans += www/$(1)$(2)-service-ports.tex
cleans += www/$(1)$(2)-instructions.tex
cleans += www/references.bib
tmpls  += www/$(1)$(2).tmpl

# Should also install git-hooks/pre-commit at .git/hooks/pre-commit.
www/$(1)$(2).checked: ./git-hooks/pre-commit $(projects-repo)/$(1)/description.json
	./git-hooks/pre-commit $(1)
	touch www/$(1)$(2).checked

www/$(1)$(2)-title.tex:
	printf '%s' "$(1)" > $$@

www/$(1)$(2)-summary.tex:
	printf '%s' "$(call summary,$(1))" > $$@

# Project name without language suffix; \jobname in LaTeX has suffix.
www/$(1)$(2)-project.tex:
	printf '\\newcommand{\\project}{%s}' "$(1)" > $$@

www/$(1)$(2)-language.tex:
	printf '\\newcommand{\\lang}{%s\\xspace}' "$(2)" > $$@

www/$(1)$(2)-prerequisites.tex:
	printf '\\newcommand{\\prerequisites}{\n' > $$@
	if [ -n "$(call prerequisites,$(1))" ]; then \
		printf '\\section*{Prerequisites}\n' >> $$@; \
		printf 'Before attempting this project, you should complete the following prerequisites:\n' >> $$@; \
		printf '\\begin{enumerate}\n' >> $$@; \
		for p in $(call prerequisites,$(1)); do \
			if jq -r '.languages[] // empty' $(projects-repo)/$$$$p/description.json | grep $(2); then \
				printf '\item %s\n' "\\href{$$$${p}$(2)}{$$$${p} in $(2)}" >> $$@; \
			else \
				printf '\item %s\n' "\\href{$$$${p}}{$$$${p}}" >> $$@; \
			fi; \
		done; \
		printf '\\end{enumerate}\n' >> $$@; \
	fi
	printf '}' >> $$@; \

www/$(1)$(2)-extension.tex:
	printf '\\newcommand{\\extension}{%s}' "$(extension_$(2))" > $$@

www/$(1)$(2)-service-ip.tex:
	printf '\\newcommand{\\serviceip}{%s\\xspace}' "$(service_ip)" > $$@

www/$(1)$(2)-service-ports.tex: $(projects-repo)/$(p)/description.json
	touch $$@
	alphabet="abcdefghijklmnopqrstuvwxyz"; \
	n=1; \
	for p in $(call service_ports,$(1)); do \
		l=$$$$(expr substr "$$$${alphabet}" "$$$${n}" 1); \
		printf '\\newcommand{\\serviceport%s}{%s\\xspace}' "$$$${l}" "$$$${p}" >> $$@; \
		n="$$$$((n + 1))"; \
	done

ifneq ($(wildcard $(projects-repo)/$(1)/instructions.md),)
www/$(1)$(2)-instructions.tex: $(projects-repo)/$(1)/instructions.md $(if $(call service_sources,$(1)),$(foreach s,$(call service_sources,$(1)),www/$(s)))
	if [ -f $(projects-repo)/references.bib ]; then \
		cp $(projects-repo)/references.bib www/; \
	fi
	pandoc -f markdown_strict -t latex $(projects-repo)/$(1)/instructions.md >$$@
else
www/$(1)$(2)-instructions.tex: $(projects-repo)/$(1)/instructions.tex $(if $(call service_sources,$(1)),$(foreach s,$(call service_sources,$(1)),www/$(s)))
	if [ -f $(projects-repo)/references.bib ]; then \
		cp $(projects-repo)/references.bib www/; \
	fi
	cp $(projects-repo)/$(1)/instructions.tex $$@
endif

www/$(1)$(2).tex: www/$(1)$(2).checked \
                  www/$(1)$(2)-title.tex \
                  www/$(1)$(2)-summary.tex \
                  www/$(1)$(2)-project.tex \
                  www/$(1)$(2)-language.tex \
                  www/$(1)$(2)-prerequisites.tex \
                  www/$(1)$(2)-extension.tex \
                  www/$(1)$(2)-service-ip.tex \
                  www/$(1)$(2)-service-ports.tex \
                  www/$(1)$(2)-instructions.tex
	cp www/template-record.tex $$@

$(foreach s,$(call service_sources,$(1)), \
	$(eval $(call BUILDSERVICESRC_template,$(1),$(s))) \
)

endef

$(foreach p,$(projects), \
	$(foreach l,$(call languages,$(p)), \
		$(eval $(call BUILDRECORD_template,$(p),$(call langsuffix,$(l)))) \
	) \
)

define BUILDPROJLIST_template
cleans  += project-list.go

project-list.go: gen-project-list $(foreach p,$(projects),$(projects-repo)/$(p)/description.json)
	./gen-project-list >$$@
endef

$(eval $(call BUILDPROJLIST_template))

define BUILDREF_template
cleans += www/reference-cmd-list.tex
cleans += www/reference-c-list.tex
cleans += www/reference-go-list.tex
cleans += www/reference-python-list.tex
cleans += www/reference-fn-c-list.tex
cleans += www/reference-fn-go-list.tex
cleans += www/reference-fn-python-list.tex
cleans += www/references.tex
tmpls  += www/references.tmpl

www/reference-cmd-list.tex: $(tmpls)
	grep -h cmddesc $(patsubst %,$(projects-repo)/%/instructions.tex,$(projects)) | sort | uniq >$$@

www/reference-c-list.tex: $(tmpls)
	grep -h elemcdesc $(patsubst %,$(projects-repo)/%/instructions.tex,$(projects)) | sort | uniq >$$@

www/reference-go-list.tex: $(tmpls)
	grep -h elemgodesc $(patsubst %,$(projects-repo)/%/instructions.tex,$(projects)) | sort | uniq >$$@

www/reference-python-list.tex: $(tmpls)
	grep -h elempythondesc $(patsubst %,$(projects-repo)/%/instructions.tex,$(projects)) | sort | uniq >$$@

www/reference-fn-c-list.tex: $(tmpls)
	grep -h fncdesc $(patsubst %,$(projects-repo)/%/instructions.tex,$(projects)) | sort | uniq >$$@

www/reference-fn-go-list.tex: $(tmpls)
	grep -h fngodesc $(patsubst %,$(projects-repo)/%/instructions.tex,$(projects)) | sort | uniq >$$@

www/reference-fn-python-list.tex: $(tmpls)
	grep -h fnpythondesc $(patsubst %,$(projects-repo)/%/instructions.tex,$(projects)) | sort | uniq >$$@

www/references.tex: www/reference-cmd-list.tex \
                    www/reference-c-list.tex \
                    www/reference-go-list.tex \
                    www/reference-python-list.tex \
                    www/reference-fn-c-list.tex \
                    www/reference-fn-go-list.tex \
                    www/reference-fn-python-list.tex
	cp www/template-references.tex $$@
endef

$(eval $(call BUILDREF_template))

define BUILDTEXPAGE_template
cleans += $(1)
cleans += $(css)
cleans += $(patsubst %.tmpl,%.xml,$(1))

$(1): $(patsubst %.tmpl,%.tex,$(1)) www/top.tmpl www/menu.tmpl www/bottom.tmpl www/css/aquinas.css
	latexml     --destination=$(patsubst %.tmpl,%.xml,$(1)) $(patsubst %.tmpl,%.tex,$(1))
	latexmlpost --destination=$(1) --format=html5 --stylesheet=www/LaTeXML-html5.xsl --css css/aquinas.css $(patsubst %.tmpl,%.xml,$(1))
	mv www/*.css www/css/
	sed -i 's/QUOTEETOUQ/"/g' $(1)
	sed -i 's/href="\([^c][^s][^s].*\)\.css"/href="css\/\1.css"/g' $(1)
	sed -i '1i {{define "$(notdir $(basename $(1)))"}}' $(1)
	echo "{{end}}" >>$(1)
endef

$(foreach p,$(tmpls),$(eval $(call BUILDTEXPAGE_template,$(p))))

list: $(list)

tmpls: $(tmpls)

_tmpls: $(_tmpls)

define CLEANS_template
cleans += vm-$(1).cfg
cleans += $(1)-openwrt-x86-64-combined-ext4.img
cleans += $(if $(shell grep extra_disks openwrt-build/definitions/$(1).json),$(1)-data-*.img,)
endef

define BUILDVM_template
$(eval $(call CLEANS_template,$(1)))
realcleans += openwrt-$(1)
$(1).diffconfig = $(if $(wildcard openwrt-build/definitions/$(1).diffconfig),openwrt-build/definitions/$(1).diffconfig)

$(1)-openwrt-x86-64-combined-ext4.img: openwrt-build/definitions/$(1).json buildrunsh
	$(openwrt-build) -x \
	                 -c openwrt-build/definitions/$(1).json \
	                 -p openwrt-build/definitions/$(1).post \
	                 $$(if $$($(1).diffconfig),-d $$($(1).diffconfig)) \
	                 $(2) $(files-common) openwrt-build/files/$(1)
endef

define BUILDGO_template
programs += $(1)
cleans   += $(1)
cleans   += openwrt-build/files/$(3)/usr/$(4)/$(1)

# NOTE: CGO_ENABLED=0 results in a statically-linked program even when
# using network facilities.
$(1): $(1).go $(2)
	CGO_ENABLED=0 go build $$^

ifneq ($(3),)
installs += $(3)-$(1)

.PHONY: $(3)-$(1)
$(3)-$(1): $(1)
	install -D $(1) openwrt-build/files/$(3)/usr/$(4)/$(1)
endif
endef

define BUILDGOWASM_template
wasms  += www/wasm/$(1).wasm.gz
cleans += www/wasm/$(1).wasm.gz

www/wasm/$(1).wasm.gz: wasm-$(1).go $(2)
	GOOS=js GOARCH=wasm go build -o www/wasm/$(1).wasm $$^
	gzip -f www/wasm/$(1).wasm

endef

$(eval $(call BUILDGOWASM_template,busycrate,httpd-db.go render.go wasm-book.go wasm-challenge.go wasm-projects.go wasm-rankings.go project-list.go))

define BUILDC_template
programs += $(1)
cleans   += $(1)
cleans   += openwrt-build/files/$(3)/usr/$(4)/$(1)
$(1): $(1).c $(2)
	gcc -static -m64 -o $(1) $$^

ifneq ($(3),)
installs += $(3)-$(1)

.PHONY: $(3)-$(1)
$(3)-$(1): $(1)
	install -D $(1) openwrt-build/files/$(3)/usr/$(4)/$(1)
endif
endef

define BUILDSH_template
installs += $(3)-$(1)
cleans   += openwrt-build/files/$(2)/usr/$(3)/$(1)

.PHONY: $(3)-$(1)
$(3)-$(1): $(1)
	install -D $(1) openwrt-build/files/$(2)/usr/$(3)/$(1)
endef

define BUILDCONF_template
conf   += $(1)
cleans += $(foreach h,$(hosts),openwrt-build/files/$(h)/etc/aquinas/$(1))

.PHONY: $(1)
$(1):
	for h in $(hosts); do \
		install -D $$@ openwrt-build/files/$$$$h/etc/aquinas/$$@; \
	done
endef

define BUILDFW_template
conf   += openwrt-build/files/$(1)/etc/config/firewall.post
cleans += openwrt-build/files/$(1)/etc/config/firewall.post

openwrt-build/files/$(1)/etc/config/firewall.post:
	./aquinas-update-firewall $(projects-repo) $(1) >$$@
endef

$(foreach h,$(hosts),$(eval $(call BUILDVM_template,$(h),$($(h)_openwrt_build_opts))))

$(eval $(call BUILDGO_template,gen-project-list,common.go project.go,,))
$(eval $(call BUILDGO_template,queue,common.go project.go queue-functions.go,aquinas-git,sbin))
$(eval $(call BUILDGO_template,aquinas-enqueue,common.go project.go queue-functions.go,aquinas-git,bin))
$(eval $(call BUILDGO_template,grader,common.go project.go,aquinas-git,sbin))
$(eval $(call BUILDGO_template,httpsh,common.go project.go queue-functions.go,aquinas-git,bin))
$(eval $(call BUILDSH_template,aquinas-deploy-key,aquinas-git,sbin))
$(eval $(call BUILDSH_template,aquinas-initialize-project,aquinas-git,sbin))
$(eval $(call BUILDSH_template,aquinas-initialize-projects,aquinas-git,sbin))
$(eval $(call BUILDSH_template,aquinas-add-student-slave,aquinas-git,sbin))
$(eval $(call BUILDSH_template,aquinas-remove-student-slave,aquinas-git,sbin))
$(eval $(call BUILDSH_template,aquinas-get-ssh-authorized-keys,aquinas-git,sbin))
$(eval $(call BUILDSH_template,aquinas-sanitize,aquinas-git,bin))
$(eval $(call BUILDGO_template,buildrunsh,common.go project.go,aquinas-user,bin))
$(eval $(call BUILDGO_template,httpd,common.go email.go safe-string.go key.go project.go httpd-routes.go httpd-handlers.go httpd-db.go httpd-db-dummy.go httpd-db-filesystem.go,aquinas-www,sbin))
$(eval $(call BUILDSH_template,aquinas-update-www,aquinas-git,sbin))
$(eval $(call BUILDSH_template,aquinas-update-firewall,aquinas-git,sbin))
$(eval $(call BUILDSH_template,aquinas-add-student,aquinas-www,sbin))
$(eval $(call BUILDSH_template,aquinas-remove-student,aquinas-www,sbin))
$(eval $(call BUILDSH_template,aquinas-passwd-student,aquinas-www,sbin))
$(eval $(call BUILDC_template,chrootdrop,,aquinas-target,sbin))
$(eval $(call BUILDFW_template,aquinas-target))
$(eval $(call BUILDFW_template,aquinas-user))
$(eval $(call BUILDCONF_template,aquinas-functions))
$(eval $(call BUILDCONF_template,aquinas.json))

programs: $(programs)

wasms: $(wasms)

installs: $(installs)

conf: $(conf)

vms: $(foreach h,$(hosts),$(h)-openwrt-x86-64-combined-ext4.img)

.PHONY: publish_tmpls
publish_tmpls: $(emails) $(tmpls) $(_tmpls) www/wasm/ www/css/ www/fonts/ www/img/ www/js/
	scp -r $^ $(webadmin)@$(webhost):/www/

.PHONY: publish_programs
publish_programs: programs installs conf
	ssh root@aquinas-www.$(domain) /etc/init.d/httpd stop
	ssh root@aquinas-git.$(domain) /etc/init.d/queue stop
	for h in $(hosts); do \
		echo $$h:; \
		scp -rp openwrt-build/files/$$h/* root@$$h.$(domain):/ || exit 1; \
	done
	ssh root@aquinas-www.$(domain) /etc/init.d/httpd start
	ssh root@aquinas-git.$(domain) /etc/init.d/queue start

.PHONY: publish
publish: $(wasms) publish_tmpls publish_programs

.PHONY: clean
clean:
	rm -f $(cleans)

.PHONY: realclean
realclean: clean
	rm -rf $(realcleans)
