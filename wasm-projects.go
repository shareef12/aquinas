package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
	"syscall/js"
)

func addRadio(document js.Value, parent js.Value, category string) {
	radio := document.Call("createElement", "input")
	radio.Call("setAttribute", "type", "radio")
	radio.Call("setAttribute", "name", "language")
	radio.Call("setAttribute", "onclick", fmt.Sprintf("update('%s');", category))
	parent.Call("appendChild", radio)

	label := document.Call("createTextNode", category)
	parent.Call("appendChild", label)
}

func projects(student string) {
	grades  := []grade{}
	grades2 := map[string]bool{}
	tags := map[string]bool{}

	title := "Aquinas: Project List"
	descr := "The range of projects to complete"

	if student != "" {
		url := fmt.Sprintf("/api/grades?student=%s&project=&brief=true", student)
		resp, err := http.Get(url)
		if err != nil {
			fmt.Println(err.Error())
			return
		}
		defer resp.Body.Close()

		data, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			fmt.Println(err.Error())
			return
		}

		json.Unmarshal(data, &grades)
	}

	document := js.Global().Get("document")
	document.Set("title", title)

	t := document.Call("getElementsByClassName", "page-title").Index(0)
	t.Set("innerHTML", title)

	d := document.Call("getElementsByClassName", "page-description").Index(0)
	d.Set("innerHTML", descr)

	content := document.Call("getElementsByClassName", "main-content").Index(0)

	ul := document.Call("createElement", "ul")
	ul.Call("setAttribute", "class", "checklist")

	for _, g := range grades {
		if g.Outcome == "PASS" {
			grades2[g.Project] = true
		} else {
			grades2[g.Project] = false
		}
	}

	for _, r := range projectList {
		li := document.Call("createElement", "li")
		class1 := "project " + r.language
		class2 := "link "    + r.language
		if pass, ok := grades2[r.link]; ok {
			if pass {
				class1 += " done"
			} else {
				class1 += " failed"
			}
		}

		for _, tag := range r.tags {
			tags[tag] = true
			class1 += " " + tag
			class2 += " " + tag
		}

		li.Call("setAttribute", "class", class1)
		record := fmt.Sprintf(`<a href="%s" class="%s">%s</a>: %s`, r.link, class2, r.name, r.summary)
		li.Set("innerHTML", record)
		ul.Call("appendChild", li)
	}

	addRadio(document, content, "All")
	addRadio(document, content, "C")
	addRadio(document, content, "Go")
	addRadio(document, content, "Python")
	addRadio(document, content, "AMD64")

	for tag, _ := range tags {
		addRadio(document, content, tag)
	}

	content.Call("appendChild", ul)

	js.Global().Set("update", js.FuncOf(func (this js.Value, args []js.Value) interface{} {
		category := args[0].String()

		projects := document.Call("getElementsByClassName", "project")
		for i := 0; i < projects.Length(); i++ {
			project := projects.Index(i)
			class := project.Call("getAttribute", "class").String()

			if category == "All" ||
			   strings.Contains(class, "none") ||
			   strings.Contains(class, category) {
				project.Call("setAttribute", "style", "color: black;")
			} else {
				project.Call("setAttribute", "style", "color: lightgray;")
			}
		}

		projects = document.Call("getElementsByClassName", "link")
		for i := 0; i < projects.Length(); i++ {
			project := projects.Index(i)
			class := project.Call("getAttribute", "class").String()
			fmt.Println(class)

			if category == "All" ||
			   strings.Contains(class, "none") ||
			   strings.Contains(class, category) {
				project.Call("setAttribute", "style", "pointerEvents: auto; cursor: auto; color: #28c;")
			} else {
				project.Call("setAttribute", "style", "pointerEvents: none; cursor: default; color: lightgray;")
			}
		}

		return nil
	}))
}
