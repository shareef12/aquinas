package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"syscall/js"
)

func challenge(student string) {
	title := "Aquinas: Challenge"
	descr := "Current Aquinas challenge"

	projects := map[string]bool{
		"overflowPython":  true,
		"smashPython":     true,
		"fsvPython":       true,
		"canaryPython":    true,
		"shellcodePython": true,
		"nopPython": true,
		"ropPython": true,
	}

	students := map[string][]string{}

	resp, err := http.Get("/api/grades?student=&project=&brief=true")
	if err != nil {
		fmt.Println(err.Error())
		return
	}
	defer resp.Body.Close()

	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println(err.Error())
		return
	}

	grades := []grade{}
	json.Unmarshal(data, &grades)

	for _, g := range grades {
		if g.Outcome != "PASS" {
			continue
		}
		if projects[g.Project] {
			if _, ok := students[g.Student]; ok {
				students[g.Student] = append(students[g.Student], g.Project)
			} else {
				students[g.Student] = []string{g.Project}
			}
		}
	}

	document := js.Global().Get("document")
	document.Set("title", title)

	t := document.Call("getElementsByClassName", "page-title").Index(0)
	t.Set("innerHTML", title)

	d := document.Call("getElementsByClassName", "page-description").Index(0)
	d.Set("innerHTML", descr)

	content   := document.Call("getElementsByClassName", "main-content").Index(0)
	gradeList := document.Call("createElement", "ol")

	for k, v := range students {
		entry := document.Call("createElement", "li")
		e := fmt.Sprintf("%s solved %s.", k, renderList(v))
		if len(projects) == len(v) {
			e += " Nice work!"
		}
		entry.Call("appendChild", document.Call("createTextNode", e))
		gradeList.Call("appendChild", entry)
	}

	content.Call("appendChild", gradeList)
}
