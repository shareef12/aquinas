package main

/* This program provides a job queue which allows users to submit jobs to be
 * run as other users. Queue (1) applies application controls as to who
 * can run what as whom and (2) serializes job requests.
 *
 * Clients request jobs by communicating with the queue daemon over a Unix-
 * domain socket. The queue daemon identifies the remote user by reading
 * the UID and GID associates with the socket connection.
 *
 * Queue immediately run jobs whose command starts with "-s" (synchronous).
 */

import (
	"encoding/json"
	"fmt"
	"log/syslog"
	"net"
	"os"
	"os/user"
	"strings"
)

type job struct {
	args []string /* Command to run. */
	as     string /* User to run as (already authorized) */
	uuid   string /* UUID of running job. */
}

type jobSync struct {
	args []string /* Command to run. */
	as     string /* User to run as (already authorized) */
	uuid   string /* UUID of running job. */
}

type jobInterface interface {
	setAs(as string)
	queue() (stdout, stderr []byte, err error)
	run() (stdout, stderr []byte, err error)
}

func (j *job) setAs(as string) { j.as = as }

func (j *job) queue() (stdout, stderr []byte, err error) {
	id, err := uuid()
	if err != nil {
		return
	}

	stdout = []byte("")
	stderr = []byte(id + "\n")

	jobs <- &job{args: j.args, as: j.as, uuid: id}

	return
}

func (j *job) run() (stdout, stderr []byte, err error) {
	args := append([]string{"-u", j.as}, j.args...)
	return run(nil, nil, "sudo", args...)
}

func (j *jobSync) setAs(as string) { j.as = as }

func (j *jobSync) queue() (stdout, stderr []byte, err error) {
	args := append([]string{"-u", j.as}, j.args...)
	return run(nil, nil, "sudo", args...)
}

func (j *jobSync) run() (stdout, stderr []byte, err error) {
	return j.queue()
}

var done = make(chan bool)
var jobs = make(chan *job, 1024)

func produce() {
	logger, err := syslog.New(syslog.LOG_NOTICE, os.Args[0])
	if err != nil {
		panic(err)
	}

	os.Remove(SOCKPATH)

	l, err := net.ListenUnix("unix", &net.UnixAddr{SOCKPATH, "unix"})
	if err != nil {
		panic(err)
	}

	err = os.Chmod(SOCKPATH, 0666)
	if err != nil {
		panic(err)
	}

	defer l.Close()
	defer os.Remove(SOCKPATH)

	for {
		var buf [1024 * 10]byte
		var args    []string
		var j         jobInterface
		var stdout  []byte
		var stderr  []byte

		c, err := l.AcceptUnix()
		if err != nil {
			logger.Err("failed to accept Unix socket connection")
			continue
		}

		defer c.Close()

		n, err := c.Read(buf[:])
		if err != nil {
			logger.Err("failed to read from Unix socket connection")
			continue
		}

		ucred, err := getCreds(c)
		if err != nil {
			logger.Err("failed to identify remote user")
			continue
		}

		u, err := user.LookupId(fmt.Sprintf("%d", ucred.Uid))
		if err != nil {
			logger.Err(fmt.Sprintf("request from invalid user: %d",
			                         ucred.Uid))
			continue
		}

		str := string(buf[:n - 1]) /* Drop "\n". */
		args = strings.Split(str, "\x00")

		if args[0] == "-s" {
			args = args[1:]
			j = &jobSync{args: args}
		} else {
			j = &job{args: args}
		}

		as, ok := checkPerm(args[0], u.Username)
		if (ok) {
			msg := fmt.Sprintf("%s run %s as %s: ok\n", u.Username, args[0], as)
			logger.Notice(msg)

			j.setAs(as)

			stdout, stderr, err = j.queue()
			if err != nil {
				logger.Notice(err.Error())
			}
		} else {
			msg := fmt.Sprintf("%s run %s as %s: denied\n", u.Username, args[0], as)
			stderr = []byte(msg)
			logger.Notice(msg)
		}

		b, err := json.Marshal(result{Stdout: stdout, Stderr: stderr})
		if err != nil {
			logger.Notice(err.Error())
		}

		c.Write(b)
	}

	done <- true
}

func consume() {
	logger, err := syslog.New(syslog.LOG_NOTICE, os.Args[0])
	if err != nil {
		panic(err)
	}

	for {
		j := <-jobs
		_, _, err := j.run()
		if err != nil {
			logger.Notice(err.Error())
		}

		logger.Notice(j.uuid + " complete: " + strings.Join(j.args, " "))
	}
}

func main() {
	go consume()
	go produce()

	<-done
}
