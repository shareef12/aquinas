package main

import (
	"errors"
	"regexp"
)

type alias struct {
	*string
}

func (a *alias) UnmarshalText(text []byte) (err error) {
	_a, err := safe(string(text))
	if err != nil {
		return
	}

	a.string = &_a

	return
}

func (a *alias) String() string {
	return *(a.string)
}
