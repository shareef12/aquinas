#!/bin/sh

# Run as Git hook on aquinas-git as root.
#
# Hook which updates all user repositories when a teacher updates the
# main projects repository.

set -e

if [ -f aquinas-functions ]; then
	. ./aquinas-functions
else
	. /etc/aquinas/aquinas-functions
fi

usage() {
	error usage: "$(basename "$0")" [-s STUDENT] [-p PROJECT]
}

while :;
	do case "$1" in
		-s) students=$2; shift ;;
		-p) projects=$2; shift ;;
		*)  break ;;
	esac
	shift
done

if [ -n "$1" ]; then
	# No arguments should remain.
	usage
fi

if [ ! -d "$root/teacher/workdir/projects/" ]; then
	sudo -u teacher git clone -q "$root/teacher/projects" "$root/teacher/workdir/projects/"
else
	cd "$root/teacher/workdir/projects/"
	sudo -u teacher git pull -q
	cd -
fi

if [ ! -d "$root/teacher/workdir/records/" ]; then
	sudo -u teacher git clone -q "$root/teacher/records" "$root/teacher/workdir/records/"
else
	cd "$root/teacher/workdir/records/"
	sudo -u teacher git pull -q
	cd -
fi


if [ -z "$students" ]; then
	students=$(grep ".*:.*:.*:.*:.*Aquinas Student" /etc/passwd | awk -F : '{ print $1 }')
fi

if [ -z "$projects" ]; then
	projects=$(ls "$root/teacher/workdir/projects" | grep -E -v \(Makefile\|references.bib\))
fi

skip_deploy=
for s in $students; do
	for p in $projects; do
		aquinas-initialize-project $skip_deploy "$s" "$p"
	done
	# Do not repeat deployment for subsequent students.
	skip_deploy=-s
done

aquinas-update-firewall "$root/teacher/workdir/projects/" aquinas-target | ssh "root@aquinas-target.$domain" tee /etc/config/firewall

aquinas-update-firewall "$root/teacher/workdir/projects/" aquinas-user   | ssh "root@aquinas-user.$domain" tee /etc/config/firewall

# If we are not careful, this hangs SSH; hence -T, /dev/null, and &.
if ! ssh -T "root@aquinas-target.$domain" nohup /etc/init.d/firewall restart </dev/null >/dev/null 2>&1 \&; then
	error failed to restart firewall on aquinas-target
fi

if ! ssh -T "root@aquinas-user.$domain" nohup /etc/init.d/firewall restart </dev/null >/dev/null 2>&1 \&; then
	error failed to restart firewall on aquinas-user
fi
