#!/bin/sh

. ./test-functions

log_host=root@$logServer
log_path=$root/var/log/messages
student=test@example.com
dir=$(mktemp -d)
tmp=$(mktemp)
rand=$RANDOM

trap "/bin/rm -rf $dir $tmp" SIGTERM SIGINT EXIT
trap "ssh root@aquinas-www.$domain sudo -u http aquinas-remove-student $student >/dev/null 2>&1" SIGTERM SIGINT EXIT

# Do not capture error; just cleaning up/should not already exist.
ssh root@aquinas-www.$domain sudo -u http aquinas-remove-student $student >/dev/null 2>&1

# This should fail due to example.com domain. See httpd.go which
# is written to log what we need, but not proceed further in the
# process.
error=$(wget --quiet -O /dev/null --post-data "student=$student" http://aquinas-www.$domain/register2)
if [ $? != 8 ]; then
	failmsg "$0" ${error:-no stderr}
fi

# NOTE: will fail if more than 1000 log messages between above and below.

log_msg=$(ssh $log_host tail -n 1000 $log_path | grep -a "preparing register email student: $student" | tail -1)
failbad $? "$0" "could not find \"preparing register email student: $student\" in log"

nonce=$(echo $log_msg | awk '{ print $12 }')
token=$(echo $log_msg | awk '{ print $14 }')
password=$(echo $log_msg | awk '{ print $16 }')

error=$(wget --quiet -O "$tmp" --post-data "student=$student&nonce=$nonce&token=$token&password=$password" http://aquinas-www.$domain/register3)
failbad $? "$0" ${error:-no stderr}

line=$(grep job-uuid "$tmp")
failbad $? "$0" ${line:-failed to find UUID}

uuid=$(echo "$line" | cut -d "\"" -f 6)
failbad $? "$0" ${uuid:-failed process UUID}

error=$(waitlog 600 root@$logServer "$uuid" /mnt/xvdb/var/log/messages 2>&1)
failbad $? "$0" ${error:-no stderr}

key=$(urlencode "$(cat ~/.ssh/id_rsa.pub)")
error=$(wget --quiet -O "$tmp" --http-user=test@example.com --http-password=$password --auth-no-challenge --post-data "key=$key" http://aquinas-www.$domain/ssh2)
failbad $? "$0" ${error:-no stderr}

line=$(grep job-uuid "$tmp")
failbad $? "$0" ${line:-failed to find UUID}

uuid=$(echo "$line" | cut -d "\"" -f 6)
failbad $? "$0" ${uuid:-failed process UUID}

error=$(waitlog 600 root@$logServer "$uuid" /mnt/xvdb/var/log/messages 2>&1)
failbad $? "$0" ${error:-no stderr}

norm=$(calcnorm $student)
error=$(git clone -q $norm@aquinas-git.$domain:$root/$student/git $dir 2>&1)
failbad $? "$0" ${error:-no stderr}

pushd $dir >/dev/null

cat >NOTES <<EOF
In case of fire: git commit, git push, and leave the building!
EOF

echo $rand >rand

git add NOTES
git add rand
git commit -q -s -m "Another revision for testing: $rand ($0)" >/dev/null

uuid=$(git push -q 2>&1 | cut -d " " -f 2)
failbad $? "$0" ${uuid:-no stderr}

error=$(waitlog 600 root@$logServer "$uuid" /mnt/xvdb/var/log/messages 2>&1)
failbad $? "$0" ${error:-no stderr}

hash=$(git log -1 --pretty=%H)

popd >/dev/null

record=$root/teacher/workdir/records/git/$student.record
error=$(ssh root@aquinas-git.$domain grep -q $hash.*PASS $record 2>&1)
failbad $? "$0" ${error:-no stderr}

passmsg "$0" okay
