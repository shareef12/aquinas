package main

type grade struct {
	Project   string `json:"project"`
	Student   string `json:"student"`
	Commit    string `json:"commit"`
	Timestamp string `json:"timestamp"`
	Outcome   string `json:"outcome"`
}

type db interface {
	addStudent(user, password string) (jobUuid string, err error)
	removeStudent(user string) (jobUuid string, err error)
	authenticate(user, password string) (err error)
	setPassword(user, password string) (err error)
	alias(user string) (alias string, err error)
	setAlias(user, alias string) (err error)
	sshAuthKeys(user string) (key string, err error)
	setSshAuthKeys(user, key string) (jobUuid string, err error)
	last(user, file string) (last string, err error)
	attempts(user string) (result map[string]string, err error)
	permitted(file, user string) error
	grades(user, student, project, brief string) (grades []grade, err error)
	isTeacher(user string) bool
}
