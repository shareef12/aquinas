package main

type dbDummy struct {
	_alias string
}

func (d *dbDummy) addStudent(user, password string) (jobUuid string, err error) {
	return uuid()
}

func (d *dbDummy) removeStudent(user string) (jobUuid string, err error) {
	return uuid()
}

func (d *dbDummy) authenticate(user, password string) (err error) {
	return
}

func (d *dbDummy) setPassword(user, password string) (err error) {
	return
}

func (d *dbDummy) alias(user string) (alias string, err error) {
	return d._alias, nil
}

func (d *dbDummy) setAlias(user, alias string) (err error) {
	d._alias = alias
	return
}

func (d *dbDummy) sshAuthKeys(user string) (key string, err error) {
        return "Fake SSH key", nil
}

func (d *dbDummy) setSshAuthKeys(user, key string) (jobUuid string, err error) {
	return uuid()
}

func (d *dbDummy) last(user, file string) (last string, err error) {
	return "No error", nil
}

/* Returns a mapping of project names to "done" or "failed". */
func (d *dbDummy) attempts(user string) (result map[string]string, err error) {
	return
}

func (d *dbDummy) permitted(file, user string) error {
	return nil
}

func (d *dbDummy) applyAlias(user, student string) string {
	if d._alias != "" {
		if d.isTeacher(user) {
			return d._alias + " (" + student + ")"
		} else {
			return d._alias
		}
	} else {
		if d.isTeacher(user) {
			return student
		} else {
			return "Anonymous"
		}
	}
}

func (d *dbDummy) grades(user, student, project, brief string) (grades []grade, err error) {
	return []grade{
		grade{
			Project:   "project1",
			Student:    d.applyAlias(user, "No Alias"),
			Commit:    "commit1",
			Timestamp: "timestamp1",
			Outcome:   "PASS",
		},
		grade{
			Project:   "project2",
			Student:    d.applyAlias(user, "No Alias"),
			Commit:    "commit1",
			Timestamp: "timestamp1",
			Outcome:   "PASS",
		},
		grade{
			Project:   "project1",
			Student:   "student1",
			Commit:    "commit1",
			Timestamp: "timestamp1",
			Outcome:   "PASS",
		},
		grade{
			Project:   "project2",
			Student:   "student1",
			Commit:    "commit2",
			Timestamp: "timestamp2",
			Outcome:   "FAIL",
		},
		grade{
			Project:   "project1",
			Student:   "student2",
			Commit:    "commit3",
			Timestamp: "timestamp3",
			Outcome:   "PASS",
		},
	}, nil
}

func (d *dbDummy) isTeacher(user string) bool {
	return true
}
