package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"sort"
	"syscall/js"
)

func rankings(student string) {
	title := "Aquinas: Rankings"
	descr := "Student rankings"

	resp, err := http.Get("/api/grades?student=&project=&brief=true")
	if err != nil {
		fmt.Println(err.Error())
		return
	}
	defer resp.Body.Close()

	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println(err.Error())
		return
	}

	grades := []grade{}
	json.Unmarshal(data, &grades)

	sums := make(map[string]int)
	for _, g := range grades {
		if g.Outcome != "PASS" {
			continue
		}
		if _, ok := sums[g.Student]; ok {
			sums[g.Student] += 1
		} else {
			sums[g.Student]  = 1
		}
	}

	rankings := make(map[int][]string)
	for k, v := range sums {
		if _, ok := rankings[v]; ok {
			rankings[v] = append(rankings[v], k)
		} else {
			rankings[v] = []string{k}
		}
	}

	keys := []int{}
	for k := range rankings {
		keys = append(keys, k)
	}
	sort.Sort(sort.Reverse(sort.IntSlice(keys)))

	document := js.Global().Get("document")
	document.Set("title", title)

	t := document.Call("getElementsByClassName", "page-title").Index(0)
	t.Set("innerHTML", title)

	d := document.Call("getElementsByClassName", "page-description").Index(0)
	d.Set("innerHTML", descr)

	content   := document.Call("getElementsByClassName", "main-content").Index(0)
	gradeList := document.Call("createElement", "ol")

	for _, k := range keys {
		plural := ""
		if k != 1 {
			plural = "s"
		}
		entry := document.Call("createElement", "li")
		e := fmt.Sprintf("%s with %d solution%s.", renderList(rankings[k]), k, plural)
		entry.Call("appendChild", document.Call("createTextNode", e))
		gradeList.Call("appendChild", entry)
	}

	content.Call("appendChild", gradeList)
}
