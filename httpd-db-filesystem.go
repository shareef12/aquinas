package main

import (
	"bufio"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"path"
	"path/filepath"
	"strings"
	"time"
)

type file struct {
	contents  string
	timestamp time.Time
}

type record struct {
	sshAuthKeys string
	alias       file
	anonId      int
	last        file
	records     file
}

type dbFilesystem map[string]*record

var nextId int
var errAliasExists error = errors.New("alias exists")

func (d dbFilesystem) addStudent(user, password string) (jobUuid string, err error) {
	u, _, err := run(nil, nil, "aquinas-add-student", user, password)
	if err != nil {
		return
	}

	jobUuid = string(u[:len(u)-1]) /* Drop '\n'. */

	return
}

func (d dbFilesystem) removeStudent(user string) (jobUuid string, err error) {
	u, _, err := run(nil, nil, "aquinas-remove-student", user)
	if err != nil {
		return
	}

	jobUuid = string(u[:len(u)-1]) /* Drop '\n'. */

	delete(d, user)

	return
}

func (d dbFilesystem) passwordHash(user string) (hash string, err error) {
	passwordPath := path.Join("/etc/httpd/accounts", user, "password")

	password, err := ioutil.ReadFile(passwordPath)
	if err != nil {
		return
	}

	hash = string(password[:len(password) - 1]) /* Drop "\n". */
	return
}

func (d dbFilesystem) authenticate(user, password string) (err error) {
	hashedPassword, err := d.passwordHash(user)
	if err != nil {
		err = errors.New("received bad login credentials: " +
		                 "could not read password for " + user)
		return
	}

	if !hashSame(hashedPassword, hashCalc(password)) {
		err = errors.New("received bad login credentials: bad " +
		                 "password for " + user)
		return
	}

	return
}

func (d dbFilesystem) setPassword(user, password string) (err error) {
	userPath := path.Join("/etc/httpd/accounts", user)
	if _, err = os.Stat(userPath); os.IsNotExist(err) {
		err = os.Mkdir(userPath, 0750)
	}
	if err != nil {
		return
	}

	hash := hashCalc(password)

	passwordPath := path.Join(userPath, "password")
	err = ioutil.WriteFile(passwordPath, []byte(hash + "\n"), 0644)
	return
}

func (d dbFilesystem) alias(user string) (alias string, err error) {
	aliasPath := path.Join("/etc/httpd/accounts", user, "alias")
	info, err := os.Stat(aliasPath)
	if err == nil {
		r, found := d[user]
		if !found {
			d[user] = new(record)
		} else if r.alias.timestamp.Equal(info.ModTime()) {
			return r.alias.contents, nil
		}
	} else if !os.IsNotExist(err) {
		return "", fmt.Errorf("alias stat failed: %v", err)
	} else {
		return "", nil
	}

	_alias, err := ioutil.ReadFile(aliasPath)
	if err != nil {
		return "", fmt.Errorf("alias read failed: %v", err)
	}

	alias = string(_alias[:len(_alias) - 1]) /* Drop "\n". */
	d[user].alias.contents  = alias
	d[user].alias.timestamp = info.ModTime()

	return
}

func (d dbFilesystem) setAlias(user, alias string) (err error) {
	/* Check to see if alias already in use. */
	dir := "/etc/httpd/accounts"
	files, err := ioutil.ReadDir(dir)
	if err != nil {
		return
	}

	for _, info := range files {
		p := path.Join(dir, info.Name(), "alias")

		if _, err = os.Stat(p); os.IsNotExist(err) {
			continue
		}

		alias2, err := ioutil.ReadFile(p)
		if err != nil {
			return err
		}

		if alias + "\n" == string(alias2) {
			return errAliasExists
		}
	}

	aliasPath := path.Join("/etc/httpd/accounts", user, "alias")
	err = ioutil.WriteFile(aliasPath, []byte(alias + "\n"), 0644)

	return
}

func (d dbFilesystem) sshAuthKeys(user string) (key string, err error) {
	if r, found := d[user]; !found {
		d[user] = new(record)
	} else if r.sshAuthKeys != "" {
		return r.sshAuthKeys, nil
	}

	stdin := strings.NewReader("ssh " + user + "\n")
	output, _, err := runSsh(stdin, nil, "aquinas-git." + conf.Domain)
	if err != nil {
		return "", fmt.Errorf("could not retrieve SSH key from aquinas-git: %v", err)
	}

	key = string(output[:len(output) - 1]) /* Drop "\n". */
	d[user].sshAuthKeys = key

        return
}

func (d dbFilesystem) setSshAuthKeys(user, key string) (jobUuid string, err error) {
	arg := user + ":" + key
	encodedArg := base64.StdEncoding.EncodeToString([]byte(arg))
	stdin := strings.NewReader("key " + encodedArg + "\n")
	u, _, err := runSsh(stdin, nil, "aquinas-git." + conf.Domain)
	if err != nil {
		return
	}

	jobUuid = string(u[:len(u)-1]) /* Drop '\n'. */

	return
}

func (d dbFilesystem) last(user, file string) (last string, err error) {
	project := strings.TrimSuffix(file, path.Ext(file)) + "-last"

	p := path.Join(*root, user, project)
	info, err := os.Stat(p)
	if err == nil {
		r, found := d[user]
		if !found {
			d[user] = new(record)
		} else if r.last.timestamp.Equal(info.ModTime()) {
			return r.last.contents, nil
		}
	} else if !os.IsNotExist(err) {
		return "", fmt.Errorf("last stat failed: %v", err)
	} else {
		return "", nil
	}

	_last, err := ioutil.ReadFile(p)
	if err != nil {
		return "", fmt.Errorf("last read failed: %v", err)
	}

	last = string(_last[:len(_last) - 1]) /* Drop "\n". */
	d[user].last.contents  = last
	d[user].last.timestamp = info.ModTime()

	return
}

/* Returns a mapping of project names to "done" or "failed". */
func (d dbFilesystem) attempts(user string) (result map[string]string, err error) {
	dir    := "/www/" + user + "/"
	suffix := "-records"
	result  = make(map[string]string)

	files, err := ioutil.ReadDir(dir)
	if os.IsNotExist(err) {
		return result, nil
	}
	if err != nil {
		return result, fmt.Errorf("attempts readdir failed: %v", err)
	}

	for _, info := range files {
		var f *os.File
		var ok bool

		if !strings.HasSuffix(info.Name(), suffix) {
			continue
		}

		proj := strings.TrimSuffix(info.Name(), suffix)

		f, err = os.Open(dir + info.Name())
		if os.IsNotExist(err) {
			err = nil
			continue
		}
		if err != nil {
			return
		}

		defer f.Close()

		scanner := bufio.NewScanner(f)
		for scanner.Scan() {
			if strings.Contains(scanner.Text(), "PASS") {
				ok = true
				break
			}
		}

		if ok {
			result[proj] = "done"
		} else {
			result[proj] = "failed"
		}
	}

	return
}

func (d dbFilesystem) permitted(file, user string) error {
	if file == "" {
		/* Root is always permitted. */
		return nil
	}

	base := strings.TrimSuffix(file, filepath.Ext(file))
	restrict := path.Join("/etc/httpd/restrictions", base)

	files, err := ioutil.ReadDir(restrict)
	if err != nil {
		if os.IsNotExist(err) {
			return nil
		}

		return err
	}

	if user != "" {
		for _, file := range files {
			if file.Name() == user {
				return nil
			}
		}
	}

	msg := "project is restricted"
	return errors.New(msg)
}

func (d dbFilesystem) applyAlias(user, student string) string {
	alias, err := d.alias(student)
	if err != nil || alias == "" {
		if d.isTeacher(user) || user == student {
			return student
		} else {
			if _, ok := d[student]; !ok {
				nextId++
				d[student] = &record{anonId: nextId}
			} else if d[student].anonId == 0 {
				nextId++
				d[student].anonId = nextId
			}

			return fmt.Sprintf("Anonymous #%d", d[student].anonId)
		}
	} else {
		if d.isTeacher(user) || user == student {
			return alias + " (" + student + ")"
		} else {
			return alias
		}
	}
}

func (d dbFilesystem) grades(user, student, project, brief string) (grades []grade, err error) {
	var students []os.FileInfo

	/* Allowing students to restrict query based on user could violate privacy. */
	if (!d.isTeacher(student) && student == user) ||
	   ( d.isTeacher(student) && student != "") {
		s, err := os.Stat(path.Join("/www", student))
		if err != nil {
			return []grade{}, err
		}
		students = []os.FileInfo{s}
	} else {
		students, err = ioutil.ReadDir("/www")
		if err != nil {
			return []grade{}, err
		}
	}

	for _, info1 := range students {
		student2 := info1.Name()

		if student != student2 && !strings.Contains(student2, "@") {
			/* Test account, and not explicitly requested. */
			continue
		}

		records, err := ioutil.ReadDir(path.Join("/www", student2))
		if err != nil {
			return []grade{}, err
		}

		for _, info2 := range records {
			var grade2 []grade

			record := info2.Name()

			if !strings.HasSuffix(record, "-records") {
				continue
			}

			if project != "" && project != strings.TrimSuffix(record, "-records") {
				continue
			}

			f, err := os.Open(path.Join("/www", student2, record))
			if err != nil {
				logger.Debug("unable to open " + path.Join("/www", student2, record))
				continue
			}

			dec := json.NewDecoder(f)

			err = dec.Decode(&grade2)
			if err != nil {
				logger.Debug("unable to decode " + path.Join("/www", student2, record))
				continue
			}

			if brief == "true" {
				var g2 grade

				for _, g := range grade2 {
					if g.Outcome == "PASS" {
						g2 = g
						break
					} else {
						g2 = g
					}
				}

				grade2 = []grade{g2}
			}

			grades = append(grades, grade2...)
		}
	}

	for i, _ := range grades {
		grades[i].Student = d.applyAlias(user, grades[i].Student)
	}

	return grades, nil
}

func (d dbFilesystem) isTeacher(user string) bool {
	_, err := os.Stat(path.Join("/etc/httpd/accounts", user, "teacher"))
	return !os.IsNotExist(err)
}
