package main

import (
	"testing"
)

func TestSafe(t *testing.T) {
	s1 := "abcdefghijklmnopqrstuvwxyz" +
	      "ABCDEFGHIJKLMNOPQRSTUVWXYZ" +
	      "0123456789" +
	      "-@.+/ "
	s2, err := safe(s1)
	if err != nil {
		t.Error(err.Error())
	}
	if s1 != s2 {
		t.Error("safe modified " + s1)
	}
}

func TestSafeDangerous(t *testing.T) {
	s1 := ";"
	s2, err := safe(s1)
	if err == nil {
		t.Error("expected error but did not receive one")
	}
	if "" != s2 {
		t.Error("safe did not produce \"\" for dangerous input")
	}
}

func TestSafeDangerousBeginning(t *testing.T) {
	s1 := ";a"
	s2, err := safe(s1)
	if err == nil {
		t.Error("expected error but did not receive one")
	}
	if "" != s2 {
		t.Error("safe did not produce \"\" for dangerous input")
	}
}

func TestSafeDangerousMiddle(t *testing.T) {
	s1 := "a;a"
	s2, err := safe(s1)
	if err == nil {
		t.Error("expected error but did not receive one")
	}
	if "" != s2 {
		t.Error("safe did not produce \"\" for dangerous input")
	}
}

func TestSafeDangerousEnd(t *testing.T) {
	s1 := "a;"
	s2, err := safe(s1)
	if err == nil {
		t.Error("expected error but did not receive one")
	}
	if "" != s2 {
		t.Error("safe did not produce \"\" for dangerous input")
	}
}
