package main

import (
	"fmt"
)

func renderList(ss []string) (s string) {
	s = ""

	if ss == nil || len(ss) == 0 {
		s += "no one"
		return
	}

	l := len(ss)
	if l >= 10 {
		s += fmt.Sprintf("%d students", l)
		return
	}

	for i := 0; i < l; i++ {
		if i == 0 {
			s += fmt.Sprintf("%s", ss[0])
		} else if i == l - 1 {
			if l > 2 {
				s += fmt.Sprintf(", and %s", ss[i])
			} else {
				s += fmt.Sprintf(" and %s", ss[i])
			}
		} else {
			s += fmt.Sprintf(", %s", ss[i])
		}
	}

	return
}
