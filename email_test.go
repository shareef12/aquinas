package main

import (
	"testing"
)

func TestSafeEmail(t *testing.T) {
	d := []byte("user@example.com")
	var e email

	if err := e.UnmarshalText(d); err != nil {
		t.Error(err.Error())
	}

	if e.String() != string(d) {
		t.Error("safeEmail modified")
	}
}

func TestSafeEmail2(t *testing.T) {
	d := []byte("User <user@example.com>")
	var e email

	if err := e.UnmarshalText(d); err != nil {
		t.Error(err.Error())
	}

	if e.String() != "user@example.com" {
		t.Error("safeEmail mis-parsed")
	}
}

func TestSafeEmailIllegalSpaceLocalPart(t *testing.T) {
	d := []byte("User <user @example.com>")
	var e email

	if err := e.UnmarshalText(d); err == nil {
		t.Error("safeEmail accepted space in local part")
	}
}

func TestSafeEmailIllegalSpaceDomain(t *testing.T) {
	d := []byte("User <user@example .com>")
	var e email

	if err := e.UnmarshalText(d); err == nil {
		t.Error("safeEmail accepted space in domain")
	}
}

func TestAllowedEmailDomain(t *testing.T) {
	if !allowedEmailDomain(conf.EmailSender) {
		t.Error("did not permit " + conf.EmailSender)
	}
}

func TestAllowedEmailDomainDeny(t *testing.T) {
	email := "user@example.invalid" /* RFC 2606. */
	if allowedEmailDomain(email) {
		t.Error("permitted " + email)
	}
}
