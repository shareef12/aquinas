package main

import (
	"errors"
	"regexp"
)

type safeString struct {
	*string
}

func safe(s1 string) (string, error) {
	blacklist := regexp.MustCompile("[^a-zA-Z0-9@.=+-/ ]")
	if blacklist.Match([]byte(s1)) {
		return "", errors.New("blacklisted rune in string")
	}

	return s1, nil
}

func (s *safeString) UnmarshalText(text []byte) (err error) {
	_s, err := safe(string(text))
	if err != nil {
		return
	}

	s.string = &_s

	return
}

func (s *safeString) String() string {
	return *(s.string)
}
