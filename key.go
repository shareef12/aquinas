package main

import (
	"encoding/base64"
	"errors"
	"fmt"
	"regexp"
	"strings"
)

type key struct {
	*string
}

func (k *key) UnmarshalText(text []byte) (err error) {
	s := strings.TrimSpace(string(text))
	k.string = &s

	blacklist := regexp.MustCompile("[^a-zA-Z0-9@.=+-/ \r\n]")
	if blacklist.Match([]byte(*k.string)) {
		return errors.New("blacklisted rune in key " + *k.string)
	}

	keys := strings.Split(*k.string, "\n")
	for n, key := range keys {
		field := strings.Fields(key)
		if len(field) != 3 {
			return errors.New(fmt.Sprintf("SSH key %d field count not valid", n))
		}

		if _, err := base64.StdEncoding.DecodeString(field[1]); err != nil {
			return errors.New(fmt.Sprintf("SSH key %d base64 not valid", n))
		}
	}

	return
}

func (k *key) String() string {
	return *(k.string)
}
